# eCMR Frontend

This is the frontend application of the eCMR service. The Frontend is designed to create, manage, archive sign and send an
eCMR ( digital convention relative au contrat de transport international de marchandises par route) with your partners
in road transports, including cross-border cases.

Core technology of the frontend is the [Angular](https://angular.io/) framework.

# Versions

The versions of the runtime environment and the most relevant frameworks used are listed below:

* trion/ng-cli-karma: 18.2.11 (CI Pipeline Base Image)
* Node: 22.11.0 (as used by the trion/ng-cli-karma:18.2.11 image to build the application)
* npm: 10.9.0   (as used by the given Node version)
* Angular CLI: 18.2.12

## Documentation

For more details, please refer to the `documentation` directory.

## License

Licensed under the Open Logistics Foundation License 1.3.
For details on the licensing terms, see the LICENSE file.

## Licenses of third-party dependencies

For information about licenses of third-party dependencies, please refer to the `README.md` files of the corresponding
components.

## Contact information

* Working Group Information
* Development Team
* etc.
