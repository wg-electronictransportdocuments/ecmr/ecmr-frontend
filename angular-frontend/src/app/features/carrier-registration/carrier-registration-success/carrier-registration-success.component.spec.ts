/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarrierRegistrationSuccessComponent } from './carrier-registration-success.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../../../app.component';
import { HttpClient, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ExternalUserService } from '../../ecmr-editor/ecmr-editor-service/external-user.service';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

describe('CarrierRegistrationSuccessComponent', () => {
    let component: CarrierRegistrationSuccessComponent;
    let fixture: ComponentFixture<CarrierRegistrationSuccessComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
    imports: [CarrierRegistrationSuccessComponent,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        BrowserAnimationsModule],
    providers: [
        { provide: ExternalUserService, useValue: {} },
        {
            provide: ActivatedRoute,
            useValue: {
                params: of({ id: 'someId' })
            }
        },
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
    ]
})
            .compileComponents();

        fixture = TestBed.createComponent(CarrierRegistrationSuccessComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
