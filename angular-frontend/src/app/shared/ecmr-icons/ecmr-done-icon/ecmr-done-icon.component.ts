/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component} from '@angular/core';

@Component({
  selector: 'app-ecmr-done-icon',
  standalone: true,
  imports: [],
  templateUrl: './ecmr-done-icon.component.html',
  styleUrl: './ecmr-done-icon.component.scss'
})
export class EcmrDoneIconComponent {

}
