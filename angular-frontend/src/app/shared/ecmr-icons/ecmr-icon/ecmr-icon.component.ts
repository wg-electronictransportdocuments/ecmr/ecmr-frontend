/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component} from '@angular/core';

@Component({
  selector: 'app-ecmr-icon',
  standalone: true,
  imports: [],
  templateUrl: './ecmr-icon.component.html',
  styleUrl: './ecmr-icon.component.scss'
})
export class EcmrIconComponent {

}
