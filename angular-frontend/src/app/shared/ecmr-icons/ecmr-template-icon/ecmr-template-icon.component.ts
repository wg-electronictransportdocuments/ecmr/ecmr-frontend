/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component} from '@angular/core';

@Component({
  selector: 'app-ecmr-template-icon',
  standalone: true,
  imports: [],
  templateUrl: './ecmr-template-icon.component.html',
  styleUrl: './ecmr-template-icon.component.scss'
})
export class EcmrTemplateIconComponent {

}
