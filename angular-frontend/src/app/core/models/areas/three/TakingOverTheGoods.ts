/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export interface TakingOverTheGoods {
  takingOverTheGoodsPlace: string | null;
  logisticsTimeOfArrivalDateTime: Date | null;
  logisticsTimeOfDepartureDateTime: Date | null;
}
