FROM node:22.11.0-alpine as builder

COPY angular-frontend/package.json ./
COPY angular-frontend/package-lock.json ./
RUN npm ci && mkdir /ng-app && mv ./node_modules ./ng-app/node_modules.clean
WORKDIR /ng-app
COPY angular-frontend .
RUN rm -r node_modules dist; mv node_modules.clean node_modules && npm run ng build -- --configuration=production --output-path=dist

FROM nginxinc/nginx-unprivileged
USER root
COPY nginx/default.conf /etc/nginx/conf.d/default.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /ng-app/dist/browser /usr/share/nginx/html
# Set appropriate access right to environment so that this can be set by an unprivileged user
RUN touch /usr/share/nginx/html/assets/deployment/env.js && chown nginx:nginx /usr/share/nginx/html/assets/deployment/env.js && chmod go+rw /usr/share/nginx/html/assets/deployment/env.js

USER nginx
EXPOSE 8080/tcp
CMD ["/bin/sh",  "-c",  "envsubst < /usr/share/nginx/html/assets/deployment/env.template.js > /usr/share/nginx/html/assets/deployment/env.js && exec nginx -g 'daemon off;'"]
